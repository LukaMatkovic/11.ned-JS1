//1. nacin - bez koriscenja metoda

function sortiranje(niz) {
    for(let i=1; i<niz.length; i++)
        for(let j=0; j<i; j++)

        if(niz[i]<niz[j]) {
            var k = niz[i];
            niz[i] = niz[j];
            niz[j] = k;
        }
        var a = niz[1];
        var b = niz[niz.length-2];
        var c = "Drugi najmanji broj je "+a+", a drugi najveci broj je "+b;
        return c;
}

console.log(sortiranje([13,105,75,1,344,6,5]));

//2. nacin - koriscenje metoda sort, push i join

function drugi(niz) {
    var a = niz.sort(function(n1,n2){return n1-n2}); //funkcija sortira sve elemente od najmanjeg ka najvecem
    var d = []; //prazan niz u koji cemo dodati drugi najmanji i drugi najveci element prethodnog niza
    var b = a[1]; //drugi najmanji broj niza
    d.push(b); //dodavanje drugog najmanjeg broja u niz
    var c = a[a.length-2]; //drugi najveci broj niza
    d.push(c); //dodavanje drugog najveceg broja u niz
    return d.join(); //pretvaranje niza u string
}

console.log(drugi([13,105,75,1,344,6,5]));

//jos jedan primer
var niz1 = [0, -5, 6, 9, 4, -6, 11, 2];
console.log(sortiranje(niz1));
console.log(drugi(niz1));


